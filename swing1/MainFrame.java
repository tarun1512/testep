package swing1;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class MainFrame extends JFrame{


	private TextPanel textPanel;
	private Toolbar toolBar;
	private FormPanel formPanel;
	public  MainFrame() {
		super("Hello World");
		setLayout(new BorderLayout());
		
		textPanel = new TextPanel();
		toolBar = new Toolbar();
		formPanel = new FormPanel();
		
	//	toolBar.setTextPanel(textPanel);
	
		toolBar.setStringListener(new StringListener() {

			@Override
			public void textEmitted(String text) {
				textPanel.AppendText(text);
				
			}
			
		} );
		add(toolBar, BorderLayout.NORTH);
		add(textPanel, BorderLayout.CENTER);
		add(formPanel,BorderLayout.WEST);
		
		setSize(600, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

}
