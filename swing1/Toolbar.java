package swing1;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class Toolbar extends JPanel implements ActionListener {
	private JButton helloButton;
	private JButton gByeButton;
	private StringListener textListener;
	public Toolbar() {
		
		helloButton = new JButton("Hello!!");
		gByeButton = new JButton("GoodBye");
		
		helloButton.addActionListener(this);
		gByeButton.addActionListener(this);
		
		
		setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		add(helloButton);
		add(gByeButton);
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		JButton clicked = (JButton) e.getSource();
		
		if (clicked == helloButton) {
			textListener.textEmitted("Hello!!\n");
		//	textPanel.AppendText("Hello!!!\n");
		}
		else {
		//	textPanel.AppendText("Good Bye!!!\n");
			textListener.textEmitted("GoodBye!!\n");
		}
		}


	public void setStringListener(StringListener listener) {
		this.textListener = listener;
		
	}



}